<?php

namespace App\Tests;

use Symfony\Component\HttpClient\CurlHttpClient;

class ApiClient
{
    private $client;
    private $host;
    private $bearer;

    /**
     * API constructor.
     *
     * @param string $host
     * @param string $email
     * @param string $password
     */
    public function __construct($host, $email, $password)
    {
        $this->client = new CurlHttpClient();
        $this->host = $host;
        $this->bearer = $this->login($email, $password);
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return string Bearer
     *
     * @throws
     */
    private function login($email, $password)
    {
        $response = $this->client->request('POST', $this->host.'/authentication_token', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'body' => json_encode([
                'email' => $email,
                'password' => $password,
            ]),
        ]);
        $content = $response->getContent();
        $content = json_decode($content, true);

        return $content['token'];
    }

    /**
     * @param string $path
     *
     * @return mixed
     *
     * @throws
     */
    public function get($path)
    {
        $response = $this->client->request('GET', $this->host.$path, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Accept' => 'application/ld+json',
                'Authorization' => 'Bearer '.$this->bearer,
            ],
        ]);
        $responseContent = $response->getContent();
        $responseContent = json_decode($responseContent, true);

        return $responseContent;
    }

    /**
     * @param string       $path
     * @param object|array $content
     *
     * @return mixed
     *
     * @throws
     */
    public function post($path, $content)
    {
        $response = $this->client->request('POST', $this->host.$path, [
            'headers' => [
                'Content-Type' => 'application/ld+json',
                'Accept' => 'application/ld+json',
                'Authorization' => 'Bearer '.$this->bearer,
            ],
            'body' => json_encode($content),
        ]);
        $responseContent = $response->getContent();
        $responseContent = json_decode($responseContent, true);

        return $responseContent;
    }

    /**
     * @param string       $path
     * @param object|array $content
     *
     * @return mixed
     *
     * @throws
     */
    public function patch($path, $content)
    {
        $response = $this->client->request('PATCH', $this->host.$path, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Accept' => 'application/ld+json',
                'Authorization' => 'Bearer '.$this->bearer,
            ],
            'body' => json_encode($content),
        ]);
        $responseContent = $response->getContent();
        $responseContent = json_decode($responseContent, true);

        return $responseContent;
    }

    /**
     * @param string       $path
     *
     * @return mixed
     *
     * @throws
     */
    public function delete($path)
    {
        $response = $this->client->request('DELETE', $this->host.$path, [
            'headers' => [
                'Content-Type' => 'application/merge-patch+json',
                'Accept' => 'application/ld+json',
                'Authorization' => 'Bearer '.$this->bearer,
            ],
        ]);
        $responseContent = $response->getContent();
        $responseContent = json_decode($responseContent, true);

        return $responseContent;
    }
}
