<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpClient\Exception\ClientException;

class TokenTest extends TestCase
{
    const HOST = 'http://localhost:8000';

    /**
     * Test to check that the authentication-mechanism is working.
     *
     * @throws
     */
    public function testValidLogin(): void
    {
        $client = new CurlHttpClient();
        $response = $client->request('POST', self::HOST.'/authentication_token', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'body' => json_encode([
                'email' => 'admin@local.test',
                'password' => 'password_admin',
            ]),
        ]);
        $content = $response->getContent();
        $content = json_decode($content, true);
        $this->assertArrayHasKey('token', $content);
        $this->assertArrayHasKey('refresh_token', $content);
    }

    /**
     * Test to check that invalid credentials will not result in successful authentication.
     */
    public function testInvalidLogin(): void
    {
        $this->expectException(ClientException::class);
        new ApiClient(self::HOST, 'admin@local.test', 'not_the_password');
    }

    /**
     * @dataProvider getContentOfJWTData
     */
    public function testContentOfJWT($email, $password, $roles): void
    {
        $client = new CurlHttpClient();
        $response = $client->request('POST', self::HOST.'/authentication_token', [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'body' => json_encode([
                'email' => $email,
                'password' => $password,
            ]),
        ]);
        $content = $response->getContent();
        $content = json_decode($content, true);
        $tokenParts = explode('.', $content['token']);
        $tokenHeader = json_decode(base64_decode($tokenParts[0]), true);
        $tokenPayload = json_decode(base64_decode($tokenParts[1]), true);
        // header check
        $this->assertSame('JWT', $tokenHeader['typ']);
        $this->assertSame('RS256', $tokenHeader['alg']);
        // payload check
        $this->assertSame(4, count($tokenPayload));
        $this->assertArrayHasKey('iat', $tokenPayload);
        $this->assertArrayHasKey('exp', $tokenPayload);
        $this->assertArrayHasKey('roles', $tokenPayload);
        $this->assertArrayHasKey('username', $tokenPayload);
        $this->assertSame(count($roles), count($tokenPayload['roles']));
        foreach ($roles as $i => $role) {
            $this->assertContains($role, $tokenPayload['roles']);
        }
    }

    public function getContentOfJWTData()
    {
        return [
            ['admin@local.test', 'password_admin', ['ROLE_USER', 'ROLE_ADMIN']],
            ['user@local.test', 'password_user', ['ROLE_USER']],
        ];
    }
}
