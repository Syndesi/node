<?php

namespace App\Tests;

use Exception;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    const HOST = 'http://localhost:8000';

    // -----------------------------------------------------------------------------------------------------------------
    //  GET-TESTS
    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Test to check that every user regardless of permission can access its own user entity.
     *
     * @dataProvider getUsersMeData
     *
     * @param $email
     * @param $password
     */
    public function testGetUsersMe($email, $password): void
    {
        $api = new ApiClient(self::HOST, $email, $password);
        $response = $api->get('/users/me');
        $this->assertArrayHasKey('@context', $response);
        $this->assertArrayHasKey('@id', $response);
        $this->assertArrayHasKey('@type', $response);
        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('email', $response);
        $this->assertArrayHasKey('roles', $response);
        $this->assertArrayHasKey('forename', $response);
        $this->assertArrayHasKey('surname', $response);
        $this->assertArrayHasKey('activated', $response);
    }

    public function getUsersMeData()
    {
        return [
            ['admin@local.test', 'password_admin'],
            ['moderator@local.test', 'password_moderator'],
            ['user@local.test', 'password_user'],
        ];
    }

    /**
     * Todo: restrict access to user entities
     * @throws Exception
     */
    public function testUserHasLimitedAccessToOtherUsers(): void
    {
        $this->markTestSkipped('TODO: restrict access to user entities');
        $api = new ApiClient(self::HOST, 'user@local.test', 'password_user');
        $userId = $api->get('/users/me')['id'];
        $otherUser = null;
        $otherUsers = $api->get('/users')['hydra:member'];
        while (null == $otherUser) {
            if (0 == count($otherUsers)) {
                throw new Exception('No other user found.');
            }
            $currentUser = array_shift($otherUsers);
            if ($currentUser['id'] != $userId) {
                $otherUser = $currentUser;
            }
        }
        $this->assertArrayNotHasKey('email', $otherUser);
        $this->assertArrayNotHasKey('roles', $otherUser);
        $this->assertArrayNotHasKey('activated', $otherUser);
    }
}
