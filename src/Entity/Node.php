<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=NodeRepository::class)
 */
class Node
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\ManyToMany(targetEntity=Page::class, inversedBy="nodes")
     */
    private $pages;

    /**
     * @ORM\OneToMany(targetEntity=NodeTranslation::class, mappedBy="node", orphanRemoval=true)
     */
    private $nodeTranslations;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->nodeTranslations = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return Collection|Page[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(Page $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
        }

        return $this;
    }

    public function removePage(Page $page): self
    {
        $this->pages->removeElement($page);

        return $this;
    }

    /**
     * @return Collection|NodeTranslation[]
     */
    public function getNodeTranslations(): Collection
    {
        return $this->nodeTranslations;
    }

    public function addNodeTranslation(NodeTranslation $nodeTranslation): self
    {
        if (!$this->nodeTranslations->contains($nodeTranslation)) {
            $this->nodeTranslations[] = $nodeTranslation;
            $nodeTranslation->setNode($this);
        }

        return $this;
    }

    public function removeNodeTranslation(NodeTranslation $nodeTranslation): self
    {
        if ($this->nodeTranslations->removeElement($nodeTranslation)) {
            // set the owning side to null (unless already changed)
            if ($nodeTranslation->getNode() === $this) {
                $nodeTranslation->setNode(null);
            }
        }

        return $this;
    }
}
