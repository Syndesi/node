<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LanguageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LanguageRepository::class)
 */
class Language
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="en"
     *         }
     *     }
     * )
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=LanguageTranslation::class, mappedBy="language", orphanRemoval=true)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="array",
     *             "example"={}
     *         }
     *     }
     * )
     */
    private $languageTranslations;

    public function __construct()
    {
        $this->languageTranslations = new ArrayCollection();
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|LanguageTranslation[]
     */
    public function getLanguageTranslations(): Collection
    {
        return $this->languageTranslations;
    }

    public function addLanguageTranslation(LanguageTranslation $languageTranslation): self
    {
        if (!$this->languageTranslations->contains($languageTranslation)) {
            $this->languageTranslations[] = $languageTranslation;
            $languageTranslation->setLanguage($this);
        }

        return $this;
    }

    public function removeLanguageTranslation(LanguageTranslation $languageTranslation): self
    {
        if ($this->languageTranslations->removeElement($languageTranslation)) {
            // set the owning side to null (unless already changed)
            if ($languageTranslation->getLanguage() === $this) {
                $languageTranslation->setLanguage(null);
            }
        }

        return $this;
    }
}
