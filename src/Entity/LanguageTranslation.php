<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LanguageTranslationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidV4Generator;
use Symfony\Component\Uid\Uuid;

/**
 * @ApiResource()
 * @ORM\Table(
 *     name="language_translation",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="unique_translation", columns={"language_id", "translated_language_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass=LanguageTranslationRepository::class)
 */
class LanguageTranslation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidV4Generator::class)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="accc5798-4b87-4f90-ade2-69ffd4c6f7da"
     *         }
     *     }
     * )
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class, inversedBy="languageTranslations")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/languages/en"
     *         }
     *     }
     * )
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(nullable=false, referencedColumnName="code")
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="/languages/en"
     *         }
     *     }
     * )
     */
    private $translatedLanguage;

    /**
     * @ORM\Column(type="string", length=255)
     * @ApiProperty(
     *     attributes={
     *         "openapi_context"={
     *             "type"="string",
     *             "example"="English"
     *         }
     *     }
     * )
     */
    private $name;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getTranslatedLanguage(): ?Language
    {
        return $this->translatedLanguage;
    }

    public function setTranslatedLanguage(?Language $translatedLanguage): self
    {
        $this->translatedLanguage = $translatedLanguage;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
