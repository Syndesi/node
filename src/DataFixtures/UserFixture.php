<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Security\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('user@local.test');
        $user->setForename('Demo');
        $user->setSurname('Test');
        $user->setActivated(true);
        $user->setPassword('$argon2i$v=19$m=65536,t=4,p=1$BMw36PBGR0grH9rCqA/s8Q$Hi4RARBjzEPw7BeVDKKFDFEOInLLFHGEbIqkNuzbKRY'); // password_user
        $user->setRoles([Role::USER]);
        $manager->persist($user);

        $moderator = new User();
        $moderator->setEmail('moderator@local.test');
        $moderator->setForename('Demo');
        $moderator->setSurname('Test');
        $moderator->setActivated(true);
        $moderator->setPassword('$argon2i$v=19$m=65536,t=4,p=1$Pxb2Bn6Bf5gKuNLLncbC4g$ZJF4TSenVqgoX0gw8J6SLAlo2qMaY0vdgKYGQTuN5NA'); // password_moderator
        $moderator->setRoles([Role::USER, Role::MODERATOR]);
        $manager->persist($moderator);

        $admin = new User();
        $admin->setEmail('admin@local.test');
        $admin->setForename('Demo');
        $admin->setSurname('Test');
        $admin->setActivated(true);
        $admin->setPassword('$argon2i$v=19$m=65536,t=4,p=1$fB9f9k2tmedPGPnsRqq5Gw$3VxIwrZO1gyhPGfAs07l9DLq2J68XdSJylELEakAYSA'); // password_admin
        $admin->setRoles([Role::USER, Role::ADMIN]);
        $manager->persist($admin);

        $manager->flush();

        $this->addReference('user', $user);
        $this->addReference('moderator', $moderator);
        $this->addReference('admin', $admin);
    }
}
