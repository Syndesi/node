<?php

declare(strict_types=1);

namespace App\Swagger;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SwaggerDecorator implements NormalizerInterface
{
    private NormalizerInterface $decorated;

    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }

    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        $docs['components']['schemas']['Token'] = [
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                    'example' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2MDY4NDkwODcsImV4cCI6MTYwNjg1MjY4Nywicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoiYWRtaW5AbG9jYWxob3N0LmRlIn0.kfuR7vvN3piyJ_xrTB197NnDviyMYjLNBNK0QG6FSml4NxGP4Na6YcdgDTxKtYStW83JJNxQq6czmaw1T06pkHDZ8KL_R0ykvllVruWTHt-_4_uHDrwVxSsOpa33J1D2PqcMms5x7mKXPU1mdlwfRiSUlPsUYYgHX46Fj1B7Dh8L7iXyblRvGrhQqlJyVayMRErcwb2865W04fAP4NrLJsx2Mt61Eny8anGAsjVNPPjbus6M1lHk-Wg1pldJXuJfkDE0SJoIGxYJMc4jDWtqFHwOb75nTjvPoVG1QP-NZkmhcWTnyD8Xb7W6ItCk1x_aKKb-zFl1-i4ysAopfW0q8O1hiUzA--kZvQYNYxBT2QEFeFHHYTarAB4QBA-gL1-8GmfIcMk92S7tKiMF0vg3d9RPVX1uEutZpYJPfZ5JsBiT75s51wHhCkSgNg0-IalvG2OzLeK3LoGVSdqg8ZrZkc1EQejawXya_4mBQdFLp7WH8ft3FgF309ObXALt2ygZ92gCJirQtzitDOBweCt5RfGWldWKJ69F4MyZuOUzpfJriF9K0okqhj00AAAfoJ9PYhGUI9WIUSrjq_mt6Cv-lExeK9YusBcZday8Ifi9YI7bIeAhrxB39Po-5bfY7lasOxMniTr7rGtcM4194sdHS4DjBw8FG0_pMle6aAu4l3M',
                ],
                'refresh_token' => [
                    'type' => 'string',
                    'readOnly' => true,
                    'example' => '23a94e3b6e6f376fdd5499cd4d53a4601d6c066f1e68a0ac8ad0034e5e06faef5a53b6da4bbe36d9cab86f4bf747aeca47827dc86eb364adc6b3c1a55e6bb728',
                ],
            ],
        ];

        $docs['components']['schemas']['Credentials'] = [
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'admin@localhost.de',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => '1234',
                ],
            ],
        ];
        $docs['components']['schemas']['CredentialsRefresh'] = [
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'example' => 'api',
                ],
                'refresh_token' => [
                    'type' => 'string',
                    'example' => 'api',
                ],
            ],
        ];

        $tokenDocumentation = [
            'paths' => [
                '/authentication_token' => [
                    'post' => [
                        'tags' => ['Token'],
                        'operationId' => 'postCredentialsItem',
                        'summary' => 'Get JWT token to login.',
                        'requestBody' => [
                            'description' => 'Create new JWT Token',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/Credentials',
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Get JWT token',
                                'content' => [
                                    'application/json' => [
                                        'schema' => [
                                            '$ref' => '#/components/schemas/Token',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                '/refresh_token' => [
                    'post' => [
                        'tags' => ['Token'],
                        'operationId' => 'postCredentialsRefreshItem',
                        'summary' => 'Get new JWT token to login.',
                        'requestBody' => [
                            'description' => 'Get new JWT Token',
                            'content' => [
                                'application/json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/CredentialsRefresh',
                                    ],
                                ],
                            ],
                        ],
                        'responses' => [
                            Response::HTTP_OK => [
                                'description' => 'Get JWT token',
                                'content' => [
                                    'application/json' => [
                                        'schema' => [
                                            '$ref' => '#/components/schemas/Token',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        return array_merge_recursive($docs, $tokenDocumentation);
    }
}
