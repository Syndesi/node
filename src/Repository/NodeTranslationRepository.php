<?php

namespace App\Repository;

use App\Entity\NodeTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method NodeTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method NodeTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method NodeTranslation[]    findAll()
 * @method NodeTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NodeTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NodeTranslation::class);
    }

    // /**
    //  * @return NodeTranslation[] Returns an array of NodeTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('n.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?NodeTranslation
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
