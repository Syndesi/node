<?php

namespace App\Security;

class Role
{
    /**
     * Default role for authenticated users.
     * Users can add comments to visible articles. Comments are not published by default
     */
    const USER = 'ROLE_USER';

    /**
     * Moderator has the same access as user, but can publish/hide comments.
     * Moderators have access to all unpublished content.
     */
    const MODERATOR = 'ROLE_MODERATOR';

    /**
     * Role gives unrestricted access.
     */
    const ADMIN = 'ROLE_ADMIN';
}
