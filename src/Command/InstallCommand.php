<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class InstallCommand extends Command
{
    protected static $defaultName = 'app:install';
    protected $em;
    protected $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Installs the application')
            ->setHelp('This command installs the application and writes some base values into the database.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // check for admin user
        $email = 'admin@localhost.de';
        $password = '1234';
        $admin = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
        if (!$admin) {
            // create admin user
            $admin = (new User())
                ->setEmail($email)
                ->setForename('Admin')
                ->setSurname('Default')
                ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
                //->setRegistrationComplete(true)
                ->setActivated(true);
            $admin->setPassword($this->encoder->encodePassword($admin, $password));
            $this->em->persist($admin);
            $output->writeln('Created admin account with email ['.$email.'] and password ['.$password.'].');
        } else {
            $output->writeln('Admin account already exists, skipped creation.');
        }

        $this->em->flush();
        $output->writeln('Installed');

        return 0;
    }
}
