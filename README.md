# Syndesi Node

PHP based software for general purpose websites. Uses [API Platform](https://api-platform.com/) to provide a basic interface to pages and nodes.

Currently proof of work, not useable in production.

## Installation instructions

# Cases II API

[Link zur Dokumentation](https://cases-2-api.syndesi.dev/) | [ReDoc](https://cases-2-api.syndesi.dev/?ui=re_doc) | [Open API 3.0 Json](https://cases-2-api.syndesi.dev/docs.json)

### Docker

Folgende Images sind verfügbar:

- `index.docker.io/cases2/cases-2:api-latest` für die aktuellste Variante
- `index.docker.io/cases2/cases-2:api-TAGNAME` für getagte Versionen (sofern sie bestehen)

Beispielkonfiguration für `docker-compose`:

```yaml
  cases-2-database:
    image: mysql
    container_name: cases-2-database
    restart: always
    command: --default-authentication-plugin=mysql_native_password
    environment:
      - MYSQL_ROOT_PASSWORD=1234
      - MYSQL_DATABASE=backend
      - MYSQL_USER=backend
      - MYSQL_PASSWORD=1234
    volumes:
      - /srv/docker/cases-2-docker/mysql/volume:/var/lib/mysql
      - /srv/docker/cases-2-docker/mysql/initdb:/docker-entrypoint-initdb.d:ro

  cases-2-api:
    image: index.docker.io/cases2/cases-2:api-latest
    container_name: cases-2-api
    depends_on:
      - cases-2-database
    restart: always
    volumes:
      - /srv/docker/cases-2-docker/api/media:/var/www/public/media
      - /srv/docker/cases-2-docker/api/.env:/var/www/.env:ro
      - /srv/docker/cases-2-docker/api/private.pem:/var/www/config/jwt/private.pem:ro
      - /srv/docker/cases-2-docker/api/public.pem:/var/www/config/jwt/public.pem:ro
    links:
      - cases-2-database:cases-2-database
```

### Notwendige externe Dateien

#### 1. Ordner "media"

Der Ordner `media` ist der Ort wo die API alle hochzuladenden Dateien speichert. Wird dieser Ordner nicht definiert,
speichert Docker alle Dokumente in einem temporären Ordner und hochgeladene Dokumente gehen bei Neustarts, Updates etc.
verloren.

#### 2. .env

Die `.env`-Datei ist eine Konfigurationsdatei, als Beispiel kann die `.env.docker` genutzt werden. Die wichtigsten zu
ändernden Variablen sind folgende:

- `APP_ENV`: Die Ausführungsumgebung. `prod` ist sicherer, `dev` gibt bessere Fehlermeldungen aus.
- `TRUSTED_HOSTS`: Liste von Adressen auf die der Server reagiert. **Lokale IP-Adressen müssen hier eingegeben werden**.
- `JWT_PASSPHRASE`: Das Passwort der Token-Zertifikate. Sollte geändert werden.
- `DATABASE_URL`: Zugangsdaten für die Datenbank. Sie müssen mit denen des `cases-2-database`-Containers übereinstimmen.
- `API_KEY_TWITTER`: Zugangstoken für die Twitter-API.
- `API_KEY_FACEBOOK`: Zugangstoken für die Facebook-API.
- `API_KEY_ALPHA_VANTAGE`: Zugangstoken für die [Alpha-Vantage-API](https://www.alphavantage.co/support/#api-key).

#### 3. Token-Zertifikate

```bash
jwt_passphrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
echo "$jwt_passphrase" | openssl genpkey -out ./config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
echo "$jwt_passphrase" | openssl pkey -in ./config/jwt/private.pem -passin stdin -out ./config/jwt/public.pem -pubout
```

### Befehle



```bash
php bin/console app:install                      # writes required fields into the db, e.g. the default admin user
```
