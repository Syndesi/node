FROM kibatic/symfony:7.4

RUN apt-get -qq update > /dev/null && DEBIAN_FRONTEND=noninteractive apt-get -qq -y --no-install-recommends install \
    php7.4-mysql

ENV SYMFONY_VERSION=5
RUN mkdir -p /var/www
COPY . /var/www
COPY ./.env.docker /var/www/.env
COPY ./docker/nginx.conf /etc/nginx/sites-available/default.conf
COPY ./docker/php.ini /etc/php/7.4/fpm/conf.d/
WORKDIR /var/www
RUN composer install --no-interaction -o --no-ansi
ARG CI_COMMIT_SHORT_SHA=--------
ENV API_GIT_HASH=$CI_COMMIT_SHORT_SHA
ARG CI_COMMIT_REF_NAME=master
ENV API_GIT_NAME=$CI_COMMIT_REF_NAME
ARG BUILD_DATE=$BUILD_DATE
ENV API_BUILD_DATE=$BUILD_DATE
EXPOSE 80
COPY ./docker/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
CMD ["/entrypoint.sh"]
