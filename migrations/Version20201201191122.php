<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201201191122 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE language (code VARCHAR(255) NOT NULL, PRIMARY KEY(code)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE language_translation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', language_id VARCHAR(255) NOT NULL, translated_language_id VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_D72F30DD82F1BAF4 (language_id), INDEX IDX_D72F30DDD982AFA9 (translated_language_id), UNIQUE INDEX unique_translation (language_id, translated_language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE media_object (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE node (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', status VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE node_page (node_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_D3AE3BB6460D9FD7 (node_id), INDEX IDX_D3AE3BB6C4663E4 (page_id), PRIMARY KEY(node_id, page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE node_translation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', language_id VARCHAR(255) NOT NULL, node_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', title LONGTEXT NOT NULL, summary LONGTEXT DEFAULT NULL, content LONGTEXT DEFAULT NULL, INDEX IDX_F462EE8682F1BAF4 (language_id), INDEX IDX_F462EE86460D9FD7 (node_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', status VARCHAR(255) DEFAULT NULL, created DATETIME NOT NULL, updated DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE page_translation (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', language_id VARCHAR(255) NOT NULL, title LONGTEXT NOT NULL, summary LONGTEXT DEFAULT NULL, INDEX IDX_A3D51B1DC4663E4 (page_id), INDEX IDX_A3D51B1D82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE refresh_tokens (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, UNIQUE INDEX UNIQ_9BACE7E1C74F2195 (refresh_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE slug (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', redirect_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', parent_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', slug LONGTEXT NOT NULL, full_slug LONGTEXT NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, full_slug_hash VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_989D9B629282E548 (full_slug_hash), INDEX IDX_989D9B62B42D874D (redirect_id), INDEX IDX_989D9B62C4663E4 (page_id), INDEX IDX_989D9B62727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, forename VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, activated TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE language_translation ADD CONSTRAINT FK_D72F30DD82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (code)');
        $this->addSql('ALTER TABLE language_translation ADD CONSTRAINT FK_D72F30DDD982AFA9 FOREIGN KEY (translated_language_id) REFERENCES language (code)');
        $this->addSql('ALTER TABLE node_page ADD CONSTRAINT FK_D3AE3BB6460D9FD7 FOREIGN KEY (node_id) REFERENCES node (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE node_page ADD CONSTRAINT FK_D3AE3BB6C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE node_translation ADD CONSTRAINT FK_F462EE8682F1BAF4 FOREIGN KEY (language_id) REFERENCES language (code)');
        $this->addSql('ALTER TABLE node_translation ADD CONSTRAINT FK_F462EE86460D9FD7 FOREIGN KEY (node_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE page_translation ADD CONSTRAINT FK_A3D51B1DC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE page_translation ADD CONSTRAINT FK_A3D51B1D82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (code)');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62B42D874D FOREIGN KEY (redirect_id) REFERENCES slug (id)');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62727ACA70 FOREIGN KEY (parent_id) REFERENCES slug (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE language_translation DROP FOREIGN KEY FK_D72F30DD82F1BAF4');
        $this->addSql('ALTER TABLE language_translation DROP FOREIGN KEY FK_D72F30DDD982AFA9');
        $this->addSql('ALTER TABLE node_translation DROP FOREIGN KEY FK_F462EE8682F1BAF4');
        $this->addSql('ALTER TABLE page_translation DROP FOREIGN KEY FK_A3D51B1D82F1BAF4');
        $this->addSql('ALTER TABLE node_page DROP FOREIGN KEY FK_D3AE3BB6460D9FD7');
        $this->addSql('ALTER TABLE node_translation DROP FOREIGN KEY FK_F462EE86460D9FD7');
        $this->addSql('ALTER TABLE node_page DROP FOREIGN KEY FK_D3AE3BB6C4663E4');
        $this->addSql('ALTER TABLE page_translation DROP FOREIGN KEY FK_A3D51B1DC4663E4');
        $this->addSql('ALTER TABLE slug DROP FOREIGN KEY FK_989D9B62C4663E4');
        $this->addSql('ALTER TABLE slug DROP FOREIGN KEY FK_989D9B62B42D874D');
        $this->addSql('ALTER TABLE slug DROP FOREIGN KEY FK_989D9B62727ACA70');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE language_translation');
        $this->addSql('DROP TABLE media_object');
        $this->addSql('DROP TABLE node');
        $this->addSql('DROP TABLE node_page');
        $this->addSql('DROP TABLE node_translation');
        $this->addSql('DROP TABLE page');
        $this->addSql('DROP TABLE page_translation');
        $this->addSql('DROP TABLE refresh_tokens');
        $this->addSql('DROP TABLE slug');
        $this->addSql('DROP TABLE user');
    }
}
