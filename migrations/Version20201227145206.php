<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201227145206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slug DROP FOREIGN KEY FK_989D9B62727ACA70');
        $this->addSql('ALTER TABLE slug DROP FOREIGN KEY FK_989D9B62B42D874D');
        $this->addSql('CREATE TABLE comment (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', author_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', parent_comment_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', language_id VARCHAR(255) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, content LONGTEXT NOT NULL, status VARCHAR(255) NOT NULL, INDEX IDX_9474526CC4663E4 (page_id), INDEX IDX_9474526CF675F31B (author_id), INDEX IDX_9474526CBF2AF943 (parent_comment_id), INDEX IDX_9474526C82F1BAF4 (language_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', active TINYINT(1) NOT NULL, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_user (group_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_A4C98D39FE54D947 (group_id), INDEX IDX_A4C98D39A76ED395 (user_id), PRIMARY KEY(group_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_page (group_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_3D50ED50FE54D947 (group_id), INDEX IDX_3D50ED50C4663E4 (page_id), PRIMARY KEY(group_id, page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE node_translation_media_object (node_translation_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', media_object_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_FEBB7ABDE0B87CE0 (node_translation_id), INDEX IDX_FEBB7ABD64DE5A5 (media_object_id), PRIMARY KEY(node_translation_id, media_object_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CBF2AF943 FOREIGN KEY (parent_comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (code)');
        $this->addSql('ALTER TABLE group_user ADD CONSTRAINT FK_A4C98D39FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_user ADD CONSTRAINT FK_A4C98D39A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_page ADD CONSTRAINT FK_3D50ED50FE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_page ADD CONSTRAINT FK_3D50ED50C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE node_translation_media_object ADD CONSTRAINT FK_FEBB7ABDE0B87CE0 FOREIGN KEY (node_translation_id) REFERENCES node_translation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE node_translation_media_object ADD CONSTRAINT FK_FEBB7ABD64DE5A5 FOREIGN KEY (media_object_id) REFERENCES media_object (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE slug');
        $this->addSql('ALTER TABLE page_translation ADD slug VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CBF2AF943');
        $this->addSql('ALTER TABLE group_user DROP FOREIGN KEY FK_A4C98D39FE54D947');
        $this->addSql('ALTER TABLE group_page DROP FOREIGN KEY FK_3D50ED50FE54D947');
        $this->addSql('CREATE TABLE slug (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', redirect_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', page_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', parent_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', slug LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, full_slug LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, created DATETIME NOT NULL, updated DATETIME DEFAULT NULL, full_slug_hash VARCHAR(64) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, INDEX IDX_989D9B62B42D874D (redirect_id), INDEX IDX_989D9B62C4663E4 (page_id), UNIQUE INDEX UNIQ_989D9B629282E548 (full_slug_hash), INDEX IDX_989D9B62727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62727ACA70 FOREIGN KEY (parent_id) REFERENCES slug (id)');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62B42D874D FOREIGN KEY (redirect_id) REFERENCES slug (id)');
        $this->addSql('ALTER TABLE slug ADD CONSTRAINT FK_989D9B62C4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE group_user');
        $this->addSql('DROP TABLE group_page');
        $this->addSql('DROP TABLE node_translation_media_object');
        $this->addSql('ALTER TABLE page_translation DROP slug');
    }
}
